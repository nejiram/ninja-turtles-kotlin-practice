package com.example.ninjaturtles

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        turtles_list.setOnItemClickListener { adapterView: AdapterView<*>, view1: View, i: Int, l: Long ->
            val name = when (i) {
                0 -> "Splinter"
                1 -> "Leo"
                2 -> "Don"
                3 -> "Mike"
                else -> "Ralph"
            }
            Toast.makeText(this, "Hello $name!", Toast.LENGTH_SHORT).show()
        }
    }

    fun radioButtonClick(view:View) {
        val id = when (view) {
            rb_leo -> R.drawable.tmntleo
            rb_don -> R.drawable.tmntdon
            rb_mike -> R.drawable.tmntmike
            else -> R.drawable.tmntraph
        }
        ninja.setImageResource(id)
    }
}